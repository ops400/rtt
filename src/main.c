#include <raylib.h>
#include <stdio.h>

/*Notes: Make the cordinates right on gimp then put then in here*/

const int windowWidth = 480;
const int windowHeight = 320;
const char** windowTitle = "RTT";

struct triangleStruct
{
    // int p1x, p1y, p2x, p2y, p3x, p3y;
    Vector2 p1v, p2v, p3v;
};


int main(int argc, char** argv){
    struct triangleStruct tri1;
    tri1.p1v.x = 240;
    tri1.p1v.y = 160;
    tri1.p2v.x = 200;
    tri1.p2v.y = 120;
    tri1.p3v.x = tri1.p2v.x;
    tri1.p3v.y = tri1.p1v.y;
    struct triangleStruct tri2;
    tri2.p1v.x = 21;
    tri2.p1v.y = 50;
    tri2.p2v.x = 17;
    tri2.p2v.y = 46;
    tri2.p3v.x = 17;
    tri2.p3v.y = 50;
    struct triangleStruct tri3;
    tri3.p1v.x = 87;
    tri3.p1v.y = 93;
    tri3.p2v.x = 81;
    tri3.p2v.y = 49;
    tri3.p3v.x = 81;
    tri3.p3v.y = 93;
    InitWindow(windowWidth, windowHeight, windowTitle);
    SetTargetFPS(60);
    while(!WindowShouldClose()){
        BeginDrawing();
        // DrawCircle(windowWidth/2, windowHeight/2, 50, BLUE);
        DrawTriangle(tri1.p2v, tri1.p3v, tri1.p1v, RED);
        DrawTriangle(tri2.p1v, tri2.p2v, tri2.p3v, YELLOW);
        DrawTriangle(tri3.p1v, tri3.p2v, tri3.p3v, PURPLE);
        // DrawPixelV(tri1.p1v, BLACK);
        // DrawPixelV(tri1.p2v, BLUE);
        // DrawPixelV(tri1.p3v, RED);
        // printf("tri1.p1v.x=%f\ntri1.p1v.y=%f\n",tri1.p1v.x, tri1.p1v.y);
        // printf("tri1.p2v.x=%f\ntri1.p2v.y=%f\n",tri1.p2v.x, tri1.p2v.y);
        // printf("tri1.p3v.x=%f\ntri1.p3v.y=%f\n",tri1.p3v.x, tri1.p3v.y);
        EndDrawing();
        ClearBackground(WHITE);

    }
    return 0;
}